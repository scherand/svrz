# -*- coding: utf-8 -*-
#
# Requirements:
# python-dateutil (tested with 2.8.2)
# icalendar (tested with 4.0.9)
# # pydantic (tested with 1.9.1)
# openpyxl (tested with 3.0.9)
#
# pytest (tested with 7.1.2; for development)
#
import sys
import os
import logging
import logging.config
import argparse
import json
import openpyxl
import configparser

import util
import model

from typing import List
from inspect import getsourcefile
from pathlib import Path
from datetime import datetime
from dateutil import tz
from icalendar import Calendar


def main():
    global log
    global log_conf
    global venues

    parser = argparse.ArgumentParser()
    parser.add_argument("-v",
                        "--verbose",
                        action="store_true",
                        help="be verbose (debug)")
    parser.add_argument("-t",
                        "--team",
                        help=("Id/number of the team to work with (check URL "
                              "param on easyleague.ch); this puts the "
                              "generator into 'easyleague mode' and is "
                              "mutually exclusive with `--in`"))
    parser.add_argument("-i",
                        "--in",
                        dest="in_",
                        type=str,
                        help=("full file path to the Excel file containing "
                              "the matches to load/process; this file should "
                              "be an export from Volleymanager "
                              "(https://volleymanager.volleyball.ch); this "
                              "puts the generator into 'volleymanager mode' "
                              "and is mutually exclusive with `--team`"))
    parser.add_argument("-o",
                        "--out",
                        type=str,
                        help=("full file path to the ics file to be written; "
                              "if not present, a name will be automatically "
                              "generated"))
    parser.add_argument("-u",
                        "--update",
                        type=str,
                        help=("use the existing events contained in the file "
                              "referenced by this option as a basis and "
                              "generate updated events in a new ics file "
                              "instead of creating a new ics file from "
                              "scratch"))
    parser.add_argument("--venue-id",
                        type=str,
                        help=("only calculate and output the venue id (used "
                              "in `*_data.json` for example) for the string "
                              "following the parameter; an example string "
                              "would be 'Schulhaus am Wasser, Am Wasser 55a, "
                              "8049 Zürich'"))
    args = parser.parse_args()

    if args.verbose:
        log_conf["loggers"]["__main__"]["level"] = logging.DEBUG
        log_conf["loggers"]["util"]["level"] = logging.DEBUG
    logging.config.dictConfig(log_conf)
    log = logging.getLogger(__name__)
    log.info("starting up")

    if args.venue_id:
        venue_name = args.venue_id.strip()
        log.debug("parameter --venue-id provided, calculating venue id for "
                  f"'{venue_name}'")
        venue_id = util.generate_venue_id(venue_name)
        log.info(f"generated venue_id {venue_id} for '{venue_name}'")
        print("=" * 80)
        print(f"Venue name: {venue_name}")
        print(f"Venue Id: {venue_id}")
        print("-" * 80)
        return

    mode = "vm"  # by default, assume "Volley Manager mode"
    if args.team and args.in_:
        log.error("parameters `--in` and `--team` are mutually exclusive!")
        sys.exit(2)
    elif args.team:
        mode = "el"  # if we (only) have `--team`, assume Easyleague mode

    conf_path = loc / "svrz_ics.ini"
    if conf_path.is_file():
        log.info(f"reading config from '{conf_path}'")
        default_cfg = configparser.ConfigParser()
        default_cfg.read(conf_path)
        default_team_id = int(default_cfg["default"]["team"])
        log.debug(f"read default team Id from config: '{default_team_id}'")
    else:
        log.info(f"no config file '{conf_path}', running built-in config")

    team = args.team
    if not team:
        team = default_team_id

    add_data = loc / ADDITIONAL_DATA
    if add_data.is_file():
        additional_data = _load_additional_data(add_data)
    else:
        log.debug((f"not loading additional data as '{add_data}' is not "
                   "a file"))

    if mode == "el":
        matches = fetch_match_info(team_id=team)
    else:
        f_in = Path(args.in_)
        if not f_in.is_file():
            log.error(f"'{args.in_}' is not a file, cannot read matches "
                      "from it")
            sys.exit(21)

        try:
            matches = parse_match_xlsx(f_in)
            log.info(f"read {len(matches)} matches from Excel")
        except SVRZError as ex:
            log.error(ex)
            sys.exit(1)

    fill_venues_details(matches,
                        additional_data=additional_data)

    if not args.out:
        output_name = _generate_output_name(matches)
    else:
        output_name = args.out

    if args.update:
        existing_file = Path(args.update)
        if not existing_file.is_file():
            msg = f"'{existing_file}' is not a file, aborting (--update)"
            log.error(msg)
            sys.exit(42)
        log.info(f"checking for differences against '{existing_file}'")
        with open(existing_file, "rt") as fh:
            existing_cal_s = fh.read()
        try:
            existing_cal = Calendar.from_ical(existing_cal_s)
        except ValueError:
            log.error(f"it seems that '{existing_file}' is not an ics")
            sys.exit(42)
        else:
            msg = f"successfully parsed '{existing_file}' for comparing"
            log.info(msg)
            ex_matches = cal_to_matches(existing_cal)
            msg = (f"Excel lists '{len(matches)}' matches, *.ics file "
                   f"contains '{len(ex_matches)}' matches")
            log.info(msg)
            has_change = False
            for match in matches:
                for ex_m in ex_matches:
                    if match.equivalent(ex_m):
                        log.debug(f"copy basic from buddy: '{match.uid}'")
                        match.update_basic_data(ex_m)
                        log.debug(f"updating from buddy: '{match.uid}'")
                        # has_change will be True if *any* event
                        # has a change
                        has_change = has_change or match.update(ex_m)

    # if *not* updating an existing ics file (meaning: when creating
    # a new ics file) or changes to the existing ics file were
    # detected, then we save an ics file
    if not args.update or has_change:
        ievents = []
        for match in matches:
            ievent = match.to_ievent()
            ievents.append(ievent)

        ical = _make_single_calendar(ievents)
        _save_ics(ical, output_name)
    elif args.update:
        log.info("no changes detected, not creating new ics file")

    log.info("I am done")


def parse_match_xlsx(i_in: Path) -> List[model.Match]:
    """Convert Volleymanager Excel export into list of `Matches`.

    A list of matches can be exported into XLSX from Volleymanager.
    The columns this Excel sheet has depends on the selected (visible) columns
    in the web app.
    If the Excel contains the necessary columns, it can be parsed and a list of
    matches can be created from it.
    """
    matches = []
    required_cols = ["# Spiel", "Datum/Anspielzeit",
                     "# Heimteam", "Heimteam",
                     "# Gastteam", "Gastteam",
                     "# Halle", "Halle", "Adresse Halle", "PLZ Halle",
                     "Ort Halle", "Pluscode Halle"]
    wb = openpyxl.load_workbook(filename=i_in)
    ws = wb["Sheet1"]
    row_map = {}
    for row in ws.iter_rows(min_row=1, max_row=1):
        for idx, cell in enumerate(row):
            if cell.value in required_cols:
                row_map[cell.value] = idx
                msg = (f"found column '{cell.value}' in Excel sheet at index "
                       f"{idx}")
                log.debug(msg)

    if len(required_cols) != len(row_map):
        msg = "could not find all required columns in Excel, cannot process"
        raise SVRZError(msg)
    else:
        msg = "found all required columns in Excel"
        log.info(msg)

    for row in ws.iter_rows(min_row=2):
        match_no = row[row_map["# Spiel"]].value
        match_date = row[row_map["Datum/Anspielzeit"]].value
        home_team_id = row[row_map["# Heimteam"]].value
        home_team_name = row[row_map["Heimteam"]].value
        away_team_id = row[row_map["# Gastteam"]].value
        away_team_name = row[row_map["Gastteam"]].value
        venue_id = row[row_map["# Halle"]].value
        venue_name = row[row_map["Halle"]].value
        venue_address = (f"{row[row_map['Adresse Halle']].value}, "
                         f"{row[row_map['PLZ Halle']].value} "
                         f"{row[row_map['Ort Halle']].value}")
        venue_pluscode = row[row_map["Pluscode Halle"]].value

        m = model.Match(match_no=match_no, date=match_date,
                        home_team=home_team_name, home_team_id=home_team_id,
                        away_team=away_team_name, away_team_id=away_team_id,
                        venue_name=venue_name, venue_id=venue_id,
                        venue_address=venue_address,
                        venue_pluscode=venue_pluscode)
        matches.append(m)

    return matches


def fetch_match_info(team_id: int) -> List[model.Match]:
    """"""
    log = logging.getLogger(__name__)
    fetcher = util.fetch_easyleague_html_for_team
    parser = util.parse_easyleague_html
    markup = fetcher(team_id=team_id)
    match_info_list = parser(markup)
    log.debug(f"creating match objects for {len(match_info_list)} matches")

    matches = []
    for match_info in match_info_list:
        m = model.Match(match_no=match_info.match_no,
                        date=match_info.date,
                        home_team=match_info.home_team,
                        home_team_id=match_info.home_team_id,
                        away_team=match_info.away_team,
                        away_team_id=match_info.away_team_id,
                        venue_name=match_info.venue_name,
                        venue_id=match_info.venue_id,
                        venue_address=match_info.venue_address,
                        venue_pluscode=match_info.venue_pluscode)
        matches.append(m)

    return matches


def fill_venues_details(matches: List[model.Match],
                        additional_data: dict = None):
    """Fill list of matches with missing venue information.

    If venue information is missing for a venue in the list of matches,
    try to find info in the "additional data database" and update the matches.
    """
    global venues

    if additional_data:
        venues_data = additional_data.get("venues")
    if venues_data:
        log.debug("using additional venues data")
    else:
        log.debug("no additional venues data available")

    for match in matches:
        venue_data = venues_data.get(str(match.venue_id))
        v_name = f"{match.venue_name} ({match.venue_id})"
        # if we do not have any additional data for this venue, ignore
        if not venue_data:
            log.info(f"no additional data in database for venue {v_name}")
            continue

        if not match.venue_w3w_url:
            w3w = venue_data.get("w3w")
            if w3w:
                log.debug(("setting w3w address from additional data for "
                           f"venue '{v_name}'"))
                match._venue_w3w_url = w3w
            else:
                log.info((f"no w3w address for venue '{v_name}' in "
                          "additional data"))

        if not match.venue_pluscode:
            pluscode = venue_data.get("pluscode")
            if pluscode:
                log.debug(("setting pluscode from additional data for "
                           f"venue '{v_name}'"))
                match._venue_pluscode = pluscode
            else:
                log.info((f"no pluscode address for venue '{v_name}' "
                          f"({match.venue_id}) in additional data"))


def cal_to_matches(calendar):
    """Convert an icalendar object into a list of matches"""
    matches = []
    for event in calendar.walk(name="VEVENT"):
        match_uid = event.get("uid")
        sequence = event.get("sequence")
        created = event.get("created")
        last_modified = event.get("last-modified")
        date = event.get("dtstart")
        summary = event.get("summary")
        venue_address = event.get("location")
        desc = event.get("description")
        # do NOT set the summary in the __init__ method as the setter
        # is doing magic (copying the home/away team name into
        # properties) which will be undone later in the __init__.
        # setting the summary after the object is constructed does work
        # as intended.
        match = model.Match(
            uid=match_uid,
            sequence=sequence,
            created=created,
            last_modified=last_modified,
            # summary=summary,
            date=date,
            venue_address=venue_address,
            event_description=desc)
        match.summary = summary
        log.debug(f"converted '{match_uid}' from VEVENT to dict")
        matches.append(match)
    return matches


def _make_single_calendar(events):
    """Convert a list of ievents into a single calendar"""
    log.info(f"combining '{len(events)}' events into one calendar")
    cal = Calendar()
    cal.add("prodid", "-//SVRZ ics 1.0//woohoo.ch//")
    cal.add("version", "2.0")
    cal.add("calscale", "GREGORIAN")
    for event in events:
        cal.add_component(event)
    return cal


def _generate_output_name(matches):
    """Generate a 'sensible' filename for the *.ics file"""
    id_ = _generate_identifier(matches)
    name = f"{id_}.ics"
    log.info(f"generated output name '{name}'")
    return name


def _generate_identifier(matches):
    """Generate a 'sensible' filename for the *.ics file"""
    home_team = "unknown"
    season = str(now.year)

    home_teams = {}
    min_date = datetime(2070, 2, 15, tzinfo=ZUR)
    max_date = datetime(1970, 2, 15, tzinfo=ZUR)
    # figure out home team: it's the team with the most home matches...
    # beware of "Dreifachrunden": two home matches are not necessarily
    # enough to identify the home team!
    for match in matches:
        home_count = home_teams.setdefault(match.home_team, 0)
        home_teams[match.home_team] = home_count + 1
        if match.date < min_date:
            min_date = match.date
        if match.date > max_date:
            max_date = match.date
    for team_name, home_count in home_teams.items():
        if home_count > 2:
            home_team = team_name.replace(" ", "_")
            break

    # last two digits of first year of season, underscore, last two
    # digits of second year of season
    season = f"{str(min_date.year)[2:]}_{str(max_date.year)[2:]}"

    id_ = f"{season}_{home_team}"
    log.info(f"generated Id '{id_}'")
    return id_


def _load_additional_data(add_data):
    """ """
    if not add_data:
        return {}

    log.info(f"loading additional data from '{add_data}'")
    with open(add_data, "rt") as fh:
        data = json.loads(fh.read())

    return data


def _save_ics(ical, output_fname):
    """ """
    log.info(f"saving *.ics to '{output_fname}'")
    with open(output_fname, "wb") as fh:
        fh.write(ical.to_ical())


class SVRZError(Exception):
    pass


# some config
ADDITIONAL_DATA = "svrz_ics_data.json"

# some magic
# loc = Path(os.path.dirname(os.path.abspath(__file__)))
# log_file = loc / "svrz_ics.log"
# better?
# https://stackoverflow.com/a/18489147/254868
loc = Path(os.path.abspath(getsourcefile(lambda: 0))).parent
log_file = loc / "svrz_ics.log"
ZUR = tz.gettz("Europe/Zurich")
now = datetime.now(tz=ZUR)

# some global stuff...
venues = {}
log = None
log_level = logging.INFO  # default log level (can be changed using "-v")
log_conf = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default-console': {
            # exact format is not important, this is the minimum information
            'format': '%(name)-.128s [%(levelname)-8s] %(message)s',
        },
        'default-file': {
            # exact format is not important, this is the minimum information
            'format': '%(asctime)s %(name)-.128s %(levelname)-8s %(message)s',
        },
    },
    'handlers': {
        # console logs to stderr
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default-console',
        },
        'file': {
            'level': logging.DEBUG,
            'class': 'logging.FileHandler',
            'filename': log_file,
            'formatter': 'default-file',
        },
    },
    'loggers': {
        '': {  # root logger
            'level': logging.DEBUG,
            'handlers': ['console', 'file'],
        },
        'urllib3': {
            'level': logging.INFO,
        },
        # Our application code
        '__main__': {
            'level': log_level,
        },
        'util': {
            'level': log_level,
        },
    },
}


if __name__ == "__main__":
    main()
