# -*- coding: utf-8 -*-
import pytest

from datetime import datetime
from datetime import timezone
from dateutil import tz

from icalendar import vDatetime


@pytest.fixture
def ics_for_match_no_start_time(match_no_start_time):
    # match_date = match_no_start_time.date.astimezone(timezone.utc)
    # dtstamp = vDatetime(match_date).to_ical().decode("utf-8")
    match_created = match_no_start_time.created.astimezone(timezone.utc)
    created = vDatetime(match_created).to_ical().decode("utf-8")
    match_modified = match_no_start_time.last_modified.astimezone(timezone.utc)
    last_modified = vDatetime(match_modified).to_ical().decode("utf-8")
    i = (f"BEGIN:VEVENT\r\nSUMMARY:Match VBC Innova H2 vs VBC Nicht-Innova\r\n"
         f"DTSTART;TZID=UTC;VALUE=DATE-TIME:20200527T173000Z\r\nDTEND;"
         f"TZID=UTC;VALUE=DATE-TIME:20200527T193000Z\r\nDTSTAMP;"
         f"VALUE=DATE-TIME:{created}\r\n"
         f"UID:2020-135711-42-24@woohoo.ch\r\nSEQUENCE:0\r\n"
         f"CREATED;VALUE=DATE-TIME:{created}\r\nDESCRIPTION:\r\nLAST-MODIFIED;"
         f"VALUE=DATE-TIME:{last_modified}\r\nLOCATION:Am Wasser 55a\\, "
         f"8049 Zürich\r\nTRANSP:opaque\r\nEND:VEVENT\r\n")
    return i.encode("utf-8")


@pytest.fixture
def ics_for_match_with_start_time(ics_for_match_no_start_time):
    ics_with = ics_for_match_no_start_time.decode("utf-8")
    ics_with = ics_with.replace("20200527T173000Z", "20200527T143000Z")
    ics_with = ics_with.replace("20200527T193000Z", "20200527T163000Z")
    return ics_with.encode("utf-8")


def test_match_no_starttime_to_ievent(match_no_start_time,
                                      ics_for_match_no_start_time):
    assert match_no_start_time is not None
    assert ics_for_match_no_start_time is not None
    ics = match_no_start_time.to_ievent().to_ical()
    assert ics == ics_for_match_no_start_time


def test_match_with_starttime_to_ievent(match_with_start_time,
                                        ics_for_match_with_start_time):
    assert match_with_start_time is not None
    assert ics_for_match_with_start_time is not None
    ics = match_with_start_time.to_ievent().to_ical()
    assert ics == ics_for_match_with_start_time


def test_match_from_cal(matches_from_cal):
    assert len(matches_from_cal) == 2
    m1 = matches_from_cal[0]
    m2 = matches_from_cal[1]
    start1 = datetime(year=2020,
                      month=9,
                      day=24,
                      hour=20,
                      minute=45,
                      tzinfo=tz.gettz("Europe/Zurich"))
    created = datetime(year=2020,
                       month=8,
                       day=8,
                       hour=0,
                       minute=25,
                       second=35,
                       tzinfo=tz.gettz("Europe/Zurich"))
    assert m1.uid == "2020-194643-39908-40445@woohoo.ch"
    assert m1.summary == "Match VBC Innova H2 vs VBC Züri Unterland H5"
    assert m1.date == start1
    assert m1.match_no == 194643
    assert m1.home_team == "VBC Innova H2"
    assert m1.away_team == "VBC Züri Unterland H5"
    assert m1.created == created
    assert m1.venue_address == "Am Wasser 55a, 8049 Zürich"
    assert m1.venue_map_url == ("http://maps.google.ch/?daddr="
                                "Am+Wasser+55a+8049+Zürich")
    assert m1.venue_w3w_url == "https://w3w.co/dare.acclaim.feed"
    assert m1.venue_spst_url == "https://www.sportstaetten.ch/?ID=32970"
    assert m1.venue_pluscode == "https://plus.codes/8FVC9GW3+7XX"
    assert m1.event_description == ("Map: "
            "http://maps.google.ch/?daddr=Am+Wasser+55a+8049+Zürich\n\n"
            "Sportstätte: https://www.sportstaetten.ch/?ID=32970\n\n"
            "w3w: https://w3w.co/dare.acclaim.feed\n\n"
            "Pluscode: https://plus.codes/8FVC9GW3+7XX\n")
    assert m1.sequence == 1
    start2 = datetime(year=2020,
                      month=10,
                      day=16,
                      hour=20,
                      minute=0,
                      tzinfo=tz.gettz("Europe/Zurich"))
    assert m2.uid == "2020-194640-39287-39908@woohoo.ch"
    assert m2.summary == "Match VBC Voléro Zürich 5 vs VBC Innova H2"
    assert m2.date == start2
    assert m2.match_no == 194640
    assert m2.home_team == "VBC Voléro Zürich 5"
    assert m2.away_team == "VBC Innova H2"
    assert m2.created == created
    assert m2.venue_address == "Margrit-Rainer-Strasse 5, 8050 Zürich"
    assert m2.venue_map_url == ("http://maps.google.ch/?daddr="
                                "Margrit-Rainer-Strasse+5+8050+Zürich")
    assert m2.venue_w3w_url == "https://w3w.co/dignity.twice.shells"
    assert m2.venue_spst_url == "https://www.sportstaetten.ch/?ID=31016"
    assert m2.venue_pluscode == "https://plus.codes/8FVCCG8R+G3"
    assert m2.event_description == ("Map: "
            "http://maps.google.ch/?daddr=Margrit-Rainer-Strasse+5+8050+"
            "Zürich\n\n"
            "Sportstätte: https://www.sportstaetten.ch/?ID=31016\n\n"
            "w3w: https://w3w.co/dignity.twice.shells\n\n"
            "Pluscode: https://plus.codes/8FVCCG8R+G3\n")
    assert m2.sequence == 0
    assert m2.summary == "Match VBC Voléro Zürich 5 vs VBC Innova H2"
