# -*- coding: utf-8 -*-
import logging
import pytest
import svrz_ics

from datetime import datetime
from dateutil import tz
from icalendar import Calendar

from model import Match
from svrz_ics import cal_to_matches


@pytest.fixture
def my_timezone():
    return tz.gettz("Europe/Zurich")


@pytest.fixture
def my_now(my_timezone):
    return datetime.now(tz=my_timezone)


@pytest.fixture
def match_no_start_time():
    m = Match()
    # now = datetime.now(tz=tz.gettz("Europe/Zurich"))
    start = datetime(year=2020,
                     month=5,
                     day=27,
                     # hour=16,
                     # minute=30,
                     tzinfo=tz.gettz("Europe/Zurich"))
    m.date = start
    m.match_no = 135711
    m.home_team = "VBC Innova H2"
    m.home_team_id = 42
    m.away_team = "VBC Nicht-Innova"
    m.away_team_id = 24
    m.venue_name = "Am Wasser"
    m.venue_address = "Am Wasser 55a, 8049 Zürich"
    m.venue_url = "http://www.schule-am-wasser.ch"
    m.venue_id = 52
    return m


@pytest.fixture
def match_with_start_time(match_no_start_time):
    start = datetime(year=2020,
                     month=5,
                     day=27,
                     hour=16,
                     minute=30,
                     tzinfo=tz.gettz("Europe/Zurich"))
    match_no_start_time.date = start
    return match_no_start_time


@pytest.fixture
def matches_from_cal(monkeypatch, cal_path="testdata_innova_h2.ics"):
    monkeypatch.setattr(svrz_ics, "log", log)
    with open(cal_path, "rt") as fh:
        test_cal_s = fh.read()
    test_cal = Calendar.from_ical(test_cal_s)
    ms = cal_to_matches(test_cal)
    return ms


logging.basicConfig()
log = logging.getLogger(__name__)
