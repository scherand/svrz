# SVRZ `ics` Generator

Der SVRZ `ics` Generator erstellt eine `ics`-Datei mit allen Spielterminen für
ein Volleyballteam das Mitglied des SVRZ ist.

Die Datenquelle hierfür ist entweder einer Excel-Datei, wie sie aus dem
[Volleymanager](https://volleymanager.volleyball.ch) heruntergeladen werden
kann, oder die öffentliche Liste der Spiele auf easyleague.ch.

Die Excel-Quelle funktioniert für alle Teams der regulären Meisterschaft der
Volley Region Zürich. Die easyleague Webseite funktioniert hauptsächlich für
die Teams der Zürimeisterschaft (ZM).

Die Excel-Datei lässt sich folgendermassen aus dem Volleymanager exportieren.

Unter dem Menupunkt `Spielplan/Spielbetrieb` > `Spiele` können mittels Filter
die gewünschten Spiele ausgewählt werden. Alle hier gewählten Spiele werden
schlussendlich in der `*.ics`-Datei landen.  
Wenn die gewünschten Spiele angezeigt werden, klickt man unten auf den Knopf
"DOWNLOAD XLSX". Die dadurch erstellte/heruntergeladene Excel-Datei ist der
Input für den Generator. Falls ein Update eines bestehenden Kalenders
durchgeführt werden soll, muss der aktuelle Stand des Spielplans auf dieselbe
Weise (erneut) heruntergeladen werden.

Der Generator erwartet zwingend (mindestens) die folgenden Spalten im Excel.
Über die Auswahl der angezeigten Spalten im Volleymanager ("Balken"-Symbol
oben rechts) steuert man auch die im Excel vorhandenen Spalten. Wenn also eine
Spalte im Excel fehlt, muss sie vor dem Export im Volleymanager eingeblendet
werden.

  * "# Spiel"
  * "Datum/Anspielzeit"
  * "# Heimteam"
  * "Heimteam"
  * "# Gastteam"
  * "Gastteam"
  * "# Halle"
  * "Halle"
  * "Adresse Halle"
  * "PLZ Halle"
  * "Ort Halle"
  * "Pluscode Halle"

Für Teams der ZM ist es nötig, die Team Id (im easyleague) des eigenen Teams zu
kennen. Diese Id findet man am schnellsten, indem man auf easyleague.ch den
eigenen Spielplan anzeigt und dann in der URL nach "team_id" sucht. Diese
Nummer wird dann über den `--team` Parameter beim Aufruf des Generators
angegeben (siehe weiter unten).

Der Generator kann unter gewissen Umständen Änderungen des Spielplans
als Updates erstellen. Damit kann ein schon erstellter/importierter Kalender
um die Änderungen ergänzt werden.

Die wichtigsten Bedingungen, dass das funktioniert sind:

  * die neuste, früher erstellte `ics`-Datei muss als Eingabe angegeben werden
    (das heisst, sie muss vorhanden sein)
  * die Termine dürfen im Ziel-Kalender nicht manuell verändert worden sein


## Benutzung

Dieser Abschnitt beschreibt, wie der Generator typischerweise benutzt wird.


### Erstmaliges Erstellen der `ics`-Datei

In diesem Beispiel wird die Datei `20_21_VBC_Innova_D2.ics` erstellt. Die
Quelle ist ein aus dem Volleymanager heruntergeladenes Excel-File
(`Innova-D2_Volleymanager-DataExport.xlsx`). Ein anderer Name für die Datei
könnte mit dem `--out`-Flag angegeben werden.

    python svrz_ics.py --in "Innova-D2_Volleymanager-DataExport.xlsx"
    ...
    __main__ [INFO    ] saving *.ics to '20_21_VBC_Innova_D2.ics'


In diesem Beispiel wird die Datei `22_23_VBC_Innova_H2.ics` erstellt. Die
Quelle ist der Spielplan auf easyleague.ch. Ein anderer Name für die Datei
könnte mit dem `--out`-Flag angegeben werden.

    python svrz_ics.py --team 3221
    ...
    __main__ [INFO    ] saving *.ics to '22_23_VBC_Innova_H2.ics'

### Erstellen eines Updates für eine `ics`-Datei

Im Folgenden wird die `ics`-Datei aufgrund eines Excel-Exports nachgeführt.
Für easyleague funkioniert der Prozess sinngemäss gleich (einfach mit dem
`--team` Parameter anstelle von `--in`).

Die "originaldatei" `20_21_VBC_Innova_D2.ics` muss sich im selben Verzeichnis
wie der Generator befinden oder es muss ein absoluter Pfad angegeben werden.

Achtung: Dieser Befehl *überschreibt* die bestehende Datei!

    python svrz_ics.py --in "Innova-D2_update_Volleymanager-DataExport.xlsx" --update 20_21_VBC_Innova_D2.ics
    ...
    __main__ [INFO    ] saving *.ics to '20_21_VBC_Innova_D2.ics'

Wenn die eingabe `ics`-Datei nicht überschrieben werden soll, kann ein Name
für die Ausgabedatei angegeben werden (`-o` respektive `--out` Option):

    python svrz_ics.py --in "Innova-D2_update_Volleymanager-DataExport.xlsx" -u 20_21_VBC_Innova_D2.ics -o Inno.ics
    ...
    __main__ [INFO    ] saving *.ics to 'Inno.ics'

Falls keine Änderungen gegenüber der via `--update` angegebenen `ics`-Datei
festgestellt werden, wird *keine* Ausgabedatei geschrieben. Dies ist unabhängig
von `-o/--out`.


### Hilfe

    python svrz_ics.py --help


## Installation

Für das Funktionieren braucht der Generator die folgenden vier Pakete. Die
Versionsnummern sind die, welche bei der Entwicklung verwendet worden sind,
damit funktioniert der Generator. Es ist gut möglich, dass andere Versionen
auch funktionieren.

    python-dateutil (v2.8.2)
    icalendar (v4.0.9)
    pydantic (v1.9.1)
    openpyxl (v3.0.9)
    requests (v2.28.1)
    lxml (v4.9.1)

Die entsprechenden pip-Befehle:

    pip install python-dateutil==2.8.2
    pip install icalendar==4.0.9
    pip install pydantic==1.9.1
    pip install openpyxl==3.0.9
    pip install requests==2.28.1
    pip install lxml==4.9.1


## Zusatz-Daten

Die Datei `svrz_ics_data.json` enthält zusätzliche Informationen zu den
Hallen. Aktuell sind das [What3Words-Adressen](https://what3words.com/)
und/oder [plus codes](https://plus.codes/). Der ebenfalls gespeicherte Name
dient nur dazu, dass ein Mensch einen bestimmten Eintrag schneller findet.

Diese Daten werden in den Terminen in die Beschreibung hinzugefügt und sollen
helfen, die Hallen, bzw. im Besten Fall den richtigen Eingang ins Gebäude,
einfacher zu finden.

Wie der Name der Datei suggeriert, handelt es sich um eine `JSON`-Datei. Die in
den Daten verwendete Zahl entspricht der Hallen-Id, welche sich aus der URL
zur Seite der entsprechenden Halle auslesen lässt.


### Plus Codes

Plus Codes sind
[Open Location Codes](https://en.wikipedia.org/wiki/Open_Location_Code), OLC.
Wenn man diese (via [der Karte auf der Webseite](https://plus.codes/map/))
sucht, bekommt man einen 10-stelligen Code. Dieser kann, durch hinzufügen
einer 11-te Stelle, präziser gemacht werden.

Das anzufügende Zeichen hängt davon ab, wo der präzise Ort inerhalb des durch
den 10-stelligen Code definierten Rechtecks liegt. Hier ist die Verteilung der
Zeichen "innerhalb" des Rechtecks:

|   |   |   |   |
|---|---|---|---|
| R | V | W | X |
| J | M | P | Q |
| C | F | G | H |
| 6 | 7 | 8 | 9 |
| 2 | 3 | 4 | 5 |
