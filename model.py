# -*- coding: utf-8 -*-
#
import logging
import datetime

from dateutil import tz
from datetime import timedelta
from datetime import timezone
from dateutil import parser as du_parser
from icalendar import vDatetime
from icalendar import vDDDTypes
from icalendar import Event


class Match:
    """Match objects represent individual matches

    The main purpose of match objects is to facilitate conversion
    between something that represents a match and is easy to handle and
    ical strings. The conversion goes both ways: from matches to ical
    strings and vice versa.
    """
    def __init__(
            self,
            uid=None,
            sequence=0,
            created=None,
            last_modified=None,
            match_no=None,
            summary=None,
            date=None,
            home_team=None,
            home_team_id=None,
            away_team=None,
            away_team_id=None,
            venue_name=None,
            venue_id=None,
            venue_address=None,
            venue_url=None,
            venue_w3w_url=None,
            venue_map_url=None,
            venue_spst_url=None,
            venue_pluscode=None,
            event_description=None,
            ):
        self.log = logging.getLogger(__name__)

        self.desc_attrs = ["_venue_pluscode",
                           "_venue_w3w_url",
                           "_venue_map_url",
                           "_venue_spst_url"]

        self.version = 1
        self.tz = tz.gettz("Europe/Zurich")
        self._uid = None
        self.sequence = sequence
        self._created = None
        self.created = created
        self._last_modified = None
        self.last_modified = last_modified
        self.match_no = match_no
        self.summary = summary
        self._date = None
        self.date = date
        self._home_team = None
        self.home_team = home_team
        self.home_team_id = home_team_id
        self._away_team = None
        self.away_team = away_team
        self.away_team_id = away_team_id
        self.venue_name = venue_name
        self.venue_id = venue_id
        self._venue_address = None
        self.venue_address = venue_address
        self.venue_url = venue_url
        self._venue_w3w_url = venue_w3w_url
        self._venue_map_url = venue_map_url
        self._venue_spst_url = venue_spst_url
        self._venue_pluscode = venue_pluscode
        self.event_description = event_description
        self._event_desc = None
        self.uid = uid  # needs to be "late", references away_team_id

        ZUR = tz.gettz("Europe/Zurich")
        self.now = datetime.datetime.now(tz=ZUR)

    @property
    def uid(self):
        """Generate a string suitable to be used as UID in icalendar"""
        if self._uid:
            return self._uid

        try:
            year = self.date.year
        except AttributeError:
            year = None
        match_no = self.match_no
        home_team_id = self.home_team_id
        away_team_id = self.away_team_id
        uid = f"{year}-{match_no}-{home_team_id}-{away_team_id}"
        uid = "@".join([uid, "woohoo.ch"])
        self.log.debug(f"generated UID '{uid}'")

        # only keep the uid if it has all parts
        if year and match_no and home_team_id and away_team_id:
            self._uid = uid
        return uid

    @uid.setter
    def uid(self, new_uid):
        """set and parse new uid"""
        if not new_uid:
            return
        tokens = new_uid.split("-")
        if len(tokens) == 4:
            try:
                idx = tokens[3].find("@")
                away_team_id = int(tokens[3][:idx])
            except ValueError:
                # if there is no "@" it's not one of our uids...
                return
            if not self.match_no:
                self.log.debug(f"setting match_no to '{tokens[1]}'")
                self.match_no = int(tokens[1])
            if not self.home_team_id:
                self.log.debug(f"setting home_team_id to '{tokens[2]}'")
                self.home_team_id = int(tokens[2])
            if not self.away_team_id:
                self.log.debug(f"setting away_team_id to '{away_team_id}'")
                self.away_team_id = away_team_id
        self._uid = new_uid

    @property
    def created(self):
        if self._created:
            return self._created
        else:
            return self.now

    @created.setter
    def created(self, new_created):
        if new_created:
            self._created = self.date_from_component_or_string(new_created)

    @property
    def last_modified(self):
        if self._last_modified:
            return self._last_modified
        else:
            return self.now

    @last_modified.setter
    def last_modified(self, new_last_modified):
        if new_last_modified:
            self._last_modified = self.date_from_component_or_string(
                new_last_modified)

    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, new_date):
        if new_date:
            self._date = self.date_from_component_or_string(new_date)

    @property
    def home_team(self):
        return self._home_team

    @home_team.setter
    def home_team(self, new_team):
        if new_team:
            self._home_team = new_team

    @property
    def away_team(self):
        return self._away_team

    @away_team.setter
    def away_team(self, new_team):
        if new_team:
            self._away_team = new_team

    @property
    def venue_address(self):
        return self._venue_address

    @venue_address.setter
    def venue_address(self, new_address):
        if new_address:
            self._venue_address = str(new_address)

    @property
    def venue_w3w_url(self):
        if not self._venue_w3w_url and self.event_description:
            self._parse_event_description()
        return self._venue_w3w_url

    @venue_w3w_url.setter
    def venue_w3w_url(self, new_url):
        if new_url:
            self._event_desc = None
            self._venue_w3w_url = new_url

    @property
    def venue_map_url(self):
        if not self._venue_map_url and self.event_description:
            self._parse_event_description()
        return self._venue_map_url

    @venue_map_url.setter
    def venue_map_url(self, new_url):
        if new_url:
            self._event_desc = None
            self._venue_map_url = new_url

    @property
    def venue_spst_url(self):
        if not self._venue_spst_url and self.event_description:
            self._parse_event_description()
        return self._venue_spst_url

    @venue_spst_url.setter
    def venue_spst_url(self, new_url):
        if new_url:
            self._event_desc = None
            self._venue_spst_url = new_url

    @property
    def venue_pluscode(self):
        if not self._venue_pluscode and self.event_description:
            self._parse_event_description()
        return self._venue_pluscode

    @venue_pluscode.setter
    def venue_pluscode(self, new_code):
        if new_code:
            self._event_desc = None
            self._venue_pluscode = new_code

    @property
    def event_description(self):
        if self._event_desc:
            return self._event_desc
        else:
            has_all = True

            if self._venue_map_url:
                desc = "Map: {}\n".format(self.venue_map_url)
            else:
                has_all = False
                desc = ""
            if self._venue_spst_url:
                spst = "Sportstätte: {}\n".format(self.venue_spst_url)
                if desc:
                    desc = "\n".join([desc, spst])
                else:
                    desc = spst
            else:
                has_all = False
            if self._venue_w3w_url:
                w3w = "w3w: {}\n".format(self.venue_w3w_url)
                if desc:
                    desc = "\n".join([desc, w3w])
                else:
                    desc = w3w
            else:
                has_all = False
            if self._venue_pluscode:
                pluscode = "Pluscode: {}\n".format(self.venue_pluscode)
                if desc:
                    desc = "\n".join([desc, pluscode])
                else:
                    desc = pluscode
            else:
                has_all = False

            if has_all:
                self._event_desc = desc
            return desc

    @event_description.setter
    def event_description(self, new_event_desc):
        if new_event_desc:
            self._event_desc = str(new_event_desc)
            self._parse_event_description()

    @property
    def summary(self):
        return f"Match {self.home_team} vs {self.away_team}"

    @summary.setter
    def summary(self, new_summary):
        if new_summary:
            summary = str(new_summary)
            self._home_team, self._away_team = summary[6:].split(" vs ")
            self._home_team = self._home_team.strip()
            self._away_team = self._away_team.strip()

    def __str__(self):
        return (f"{self.home_team} vs {self.away_team} on {self.date} "
                f"(#{self.match_no}) [{self.uid}]")

    def date_from_component_or_string(self, comp):
        """parse a vDatetime, vDDDTypes or string into a datetime, incl. tz"""
        dt = comp
        if isinstance(comp, (vDatetime, vDDDTypes)):
            dt = comp.dt
        elif isinstance(comp, str):
            dt = du_parser.parse(comp, dayfirst=True, yearfirst=False)
        if dt.tzinfo is None:
            dt = dt.replace(tzinfo=self.tz)
        return dt

    def to_ievent(self):
        """Convert a match dict into an ievent"""
        self.log.debug(f"converting match ({self.date}) into an event")
        e = Event()
        e.add("uid", self.uid)
        e.add("summary", self.summary)
        _start = self.date
        if not _start.hour:
            msg = f"match start date '{_start}' without a time, assuming 19:30"
            self.log.warning(msg)
            _start = _start.replace(hour=19, minute=30)
        _end = _start + timedelta(hours=2)
        # need to convert datetimes to UTC, otherwise icalendar's
        # `to_ical()` outputs something like
        #     DTSTART;TZID=CEST;VALUE=DATE-TIME:20200924T204500
        # which is not correctly interpreted by Google for example
        # (they act as if it was UTC already and are two hours off then)
        e.add("dtstart", _start.astimezone(timezone.utc))
        e.add("dtend", _end.astimezone(timezone.utc))
        # e.add("duration", "PT2H0M0S")  # two hours in icalendar-speech...
        # about DTSTAMP: https://stackoverflow.com/a/11634654/254868
        e.add("dtstamp", self.now.astimezone(timezone.utc))
        e.add("location", self.venue_address)
        e.add("description", self.event_description)
        e.add("transp", "opaque")
        e.add("sequence", self.sequence)
        e.add("created", self.created)
        e.add("last-modified", self.last_modified)
        return e

    def equivalent(self, other):
        """check if two Matches represent the same event or not"""
        if self.uid == other.uid:
            return True
        if self.match_no == other.match_no:
            return True
        if (self.date - other.date) < timedelta(days=2):
            if self.home_team_id == other.home_team_id:
                if self.away_team_id == other.away_team_id:
                    return True
        return False

    def update_basic_data(self, source):
        """update some 'basic' data from another :class:`Match`

        The `sequence`, the create date and the last modified date must
        be kept in any case when '--update' is used. This method copies
        this data from `source` to this :class:`Match`.
        """
        msg = (f"updating sequence from '{self.sequence}' to "
               f"'{source.sequence}'")
        self.log.debug(msg)
        self.sequence = source.sequence
        msg = f"updating created from '{self.created}' to '{source.created}'"
        self.log.debug(msg)
        self.created = source.created
        msg = (f"updating last_modified from '{self.last_modified}' to "
               f"'{source.last_modified}'")
        self.log.debug(msg)
        self.last_modified = source.last_modified

    def compare(self, other):
        """check if two Matches which are equivalent have a difference

        Two :class:`Match`es representing the same event can still be
        different, for example if the website was updated and one
        instance represents the state on the website while the other
        represents the "last exported" state from an *.ics file.

        Returns:
            True if the two matches differ, False otherwise
        """
        if not self.equivalent(other):
            return False

        date_diff = False
        if self.date != other.date:
            msg = f"dates differ, me: '{self.date}', other: '{other.date}'"
            self.log.info(msg)
            date_diff = True
        home_diff = False
        if self.home_team != other.home_team:
            msg = (f"home team differs, me: '{self.home_team}', other: "
                   f"'{other.home_team}'")
            self.log.info(msg)
            home_diff = True
        away_diff = False
        if self.away_team != other.away_team:
            msg = (f"away team differs, me: '{self.away_team}', other: "
                   f"'{other.away_team}'")
            self.log.info(msg)
            away_diff = True
        venue_diff = False
        if self.venue_address != other.venue_address:
            msg = (f"venue addr. differs, me: '{self.venue_address}', "
                   f"other: '{other.venue_address}'")
            self.log.info(msg)
            venue_diff = True
        venue_map_url_diff = False
        if self.venue_map_url != other.venue_map_url:
            msg = (f"map url differs, me: '{self.venue_map_url}', "
                   f"other: '{other.venue_map_url}'")
            self.log.info(msg)
            venue_map_url_diff = True
        venue_spst_url_diff = False
        if self.venue_spst_url != other.venue_spst_url:
            msg = (f"spst url differs, me: '{self.venue_spst_url}', "
                   f"other: '{other.venue_spst_url}'")
            self.log.info(msg)
            venue_spst_url_diff = True
        venue_w3w_url_diff = False
        if self.venue_w3w_url != other.venue_w3w_url:
            msg = (f"w3w url differs, me: '{self.venue_w3w_url}', "
                   f"other: '{other.venue_w3w_url}'")
            self.log.info(msg)
            venue_w3w_url_diff = True
        venue_pluscode_diff = False
        if self.venue_pluscode != other.venue_pluscode:
            msg = (f"pluscode differs, me: '{self.venue_pluscode}', "
                   f"other: '{other.venue_pluscode}'")
            self.log.info(msg)
            venue_pluscode_diff = True

        return (date_diff or home_diff or away_diff or venue_diff or
                venue_map_url_diff or venue_spst_url_diff or
                venue_w3w_url_diff or venue_pluscode_diff)

    def update(self, other):
        """update sequence number and last-modified if required"""
        diff = self.compare(other)
        if diff:
            self.sequence += 1
            self.last_modified = self.now
        return diff

    def _parse_event_description(self):
        """parse a description into its elements"""
        if not self._event_desc:
            return

        tokens = self._event_desc.split("\n")
        for token in tokens:
            if token.startswith("Map: "):
                self._venue_map_url = " ".join(token.split()[1:])
            elif token.startswith("Sportstätte: "):
                self._venue_spst_url = " ".join(token.split()[1:])
            elif token.startswith("w3w: "):
                self._venue_w3w_url = " ".join(token.split()[1:])
            elif token.startswith("Pluscode: "):
                self._venue_pluscode = " ".join(token.split()[1:])
