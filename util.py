# -*- coding: utf-8 -*-
import logging
import requests
import urllib.parse
import datetime
import dateutil
import unicodedata
import hashlib
import string

from typing import List
from collections import namedtuple
from dateutil import tz
from lxml import html


default_tz = tz.gettz("Europe/Zurich")

Matchinfo = namedtuple(
    "Matchinfo",
    [
        "match_no",
        "date",
        "home_team",
        "home_team_id",
        "away_team",
        "away_team_id",
        "venue_name",
        "venue_id",
        "venue_address",
        "venue_pluscode"])

# German stopwords from https://github.com/stopwords-iso/stopwords-de
stopwords = ["a", "ab", "aber", "ach", "acht", "achte", "achten", "achter",
             "achtes", "ag", "alle", "allein", "allem", "allen", "aller",
             "allerdings", "alles", "allgemeinen", "als", "also", "am", "an",
             "ander", "andere", "anderem", "anderen", "anderer", "anderes",
             "anderm", "andern", "anderr", "anders", "au", "auch", "auf",
             "aus", "ausser", "ausserdem", "außer", "außerdem", "b", "bald",
             "bei", "beide", "beiden", "beim", "beispiel", "bekannt",
             "bereits", "besonders", "besser", "besten", "bin", "bis",
             "bisher", "bist", "c", "d", "d.h", "da", "dabei", "dadurch",
             "dafür", "dagegen", "daher", "dahin", "dahinter", "damals",
             "damit", "danach", "daneben", "dank", "dann", "daran", "darauf",
             "daraus", "darf", "darfst", "darin", "darum", "darunter",
             "darüber", "das", "dasein", "daselbst", "dass", "dasselbe",
             "davon", "davor", "dazu", "dazwischen", "daß", "dein", "deine",
             "deinem", "deinen", "deiner", "deines", "dem", "dementsprechend",
             "demgegenüber", "demgemäss", "demgemäß", "demselben",
             "demzufolge", "den", "denen", "denn", "denselben", "der",
             "deren", "derer", "derjenige", "derjenigen", "dermassen",
             "dermaßen", "derselbe", "derselben", "des", "deshalb",
             "desselben", "dessen", "deswegen", "dich", "die", "diejenige",
             "diejenigen", "dies", "diese", "dieselbe", "dieselben", "diesem",
             "diesen", "dieser", "dieses", "dir", "doch", "dort", "drei",
             "drin", "dritte", "dritten", "dritter", "drittes", "du", "durch",
             "durchaus", "durfte", "durften", "dürfen", "dürft", "e", "eben",
             "ebenso", "ehrlich", "ei", "ei, ", "eigen", "eigene", "eigenen",
             "eigener", "eigenes", "ein", "einander", "eine", "einem",
             "einen", "einer", "eines", "einig", "einige", "einigem",
             "einigen", "einiger", "einiges", "einmal", "eins", "elf", "en",
             "ende", "endlich", "entweder", "er", "ernst", "erst", "erste",
             "ersten", "erster", "erstes", "es", "etwa", "etwas", "euch",
             "euer", "eure", "eurem", "euren", "eurer", "eures", "f",
             "folgende", "früher", "fünf", "fünfte", "fünften", "fünfter",
             "fünftes", "für", "g", "gab", "ganz", "ganze", "ganzen",
             "ganzer", "ganzes", "gar", "gedurft", "gegen", "gegenüber",
             "gehabt", "gehen", "geht", "gekannt", "gekonnt", "gemacht",
             "gemocht", "gemusst", "genug", "gerade", "gern", "gesagt",
             "geschweige", "gewesen", "gewollt", "geworden", "gibt", "ging",
             "gleich", "gott", "gross", "grosse", "grossen", "grosser",
             "grosses", "groß", "große", "großen", "großer", "großes", "gut",
             "gute", "guter", "gutes", "h", "hab", "habe", "haben", "habt",
             "hast", "hat", "hatte", "hatten", "hattest", "hattet", "heisst",
             "her", "heute", "hier", "hin", "hinter", "hoch", "hätte",
             "hätten", "i", "ich", "ihm", "ihn", "ihnen", "ihr", "ihre",
             "ihrem", "ihren", "ihrer", "ihres", "im", "immer", "in", "indem",
             "infolgedessen", "ins", "irgend", "ist", "j", "ja", "jahr",
             "jahre", "jahren", "je", "jede", "jedem", "jeden", "jeder",
             "jedermann", "jedermanns", "jedes", "jedoch", "jemand",
             "jemandem", "jemanden", "jene", "jenem", "jenen", "jener",
             "jenes", "jetzt", "k", "kam", "kann", "kannst", "kaum", "kein",
             "keine", "keinem", "keinen", "keiner", "keines", "kleine",
             "kleinen", "kleiner", "kleines", "kommen", "kommt", "konnte",
             "konnten", "kurz", "können", "könnt", "könnte", "l", "lang",
             "lange", "leicht", "leide", "lieber", "los", "m", "machen",
             "macht", "machte", "mag", "magst", "mahn", "mal", "man",
             "manche", "manchem", "manchen", "mancher", "manches", "mann",
             "mehr", "mein", "meine", "meinem", "meinen", "meiner", "meines",
             "mensch", "menschen", "mich", "mir", "mit", "mittel", "mochte",
             "mochten", "morgen", "muss", "musst", "musste", "mussten", "muß",
             "mußt", "möchte", "mögen", "möglich", "mögt", "müssen", "müsst",
             "müßt", "n", "na", "nach", "nachdem", "nahm", "natürlich",
             "neben", "nein", "neue", "neuen", "neun", "neunte", "neunten",
             "neunter", "neuntes", "nicht", "nichts", "nie", "niemand",
             "niemandem", "niemanden", "noch", "nun", "nur", "o", "ob",
             "oben", "oder", "offen", "oft", "ohne", "ordnung", "p", "q", "r",
             "recht", "rechte", "rechten", "rechter", "rechtes", "richtig",
             "rund", "s", "sa", "sache", "sagt", "sagte", "sah", "satt",
             "schlecht", "schluss", "schon", "sechs", "sechste", "sechsten",
             "sechster", "sechstes", "sehr", "sei", "seid", "seien", "sein",
             "seine", "seinem", "seinen", "seiner", "seines", "seit",
             "seitdem", "selbst", "sich", "sie", "sieben", "siebente",
             "siebenten", "siebenter", "siebentes", "sind", "so", "solang",
             "solche", "solchem", "solchen", "solcher", "solches", "soll",
             "sollen", "sollst", "sollt", "sollte", "sollten", "sondern",
             "sonst", "soweit", "sowie", "später", "startseite", "statt",
             "steht", "suche", "t", "tag", "tage", "tagen", "tat", "teil",
             "tel", "tritt", "trotzdem", "tun", "u", "uhr", "um", "und",
             "uns", "unse", "unsem", "unsen", "unser", "unsere", "unserer",
             "unses", "unter", "v", "vergangenen", "viel", "viele", "vielem",
             "vielen", "vielleicht", "vier", "vierte", "vierten", "vierter",
             "viertes", "vom", "von", "vor", "w", "wahr", "wann", "war",
             "waren", "warst", "wart", "warum", "was", "weg", "wegen", "weil",
             "weit", "weiter", "weitere", "weiteren", "weiteres", "welche",
             "welchem", "welchen", "welcher", "welches", "wem", "wen",
             "wenig", "wenige", "weniger", "weniges", "wenigstens", "wenn",
             "wer", "werde", "werden", "werdet", "weshalb", "wessen", "wie",
             "wieder", "wieso", "will", "willst", "wir", "wird", "wirklich",
             "wirst", "wissen", "wo", "woher", "wohin", "wohl", "wollen",
             "wollt", "wollte", "wollten", "worden", "wurde", "wurden",
             "während", "währenddem", "währenddessen", "wäre", "würde",
             "würden", "x", "y", "z", "z.b", "zehn", "zehnte", "zehnten",
             "zehnter", "zehntes", "zeit", "zu", "zuerst", "zugleich", "zum",
             "zunächst", "zur", "zurück", "zusammen", "zwanzig", "zwar",
             "zwei", "zweite", "zweiten", "zweiter", "zweites", "zwischen",
             "zwölf", "über", "überhaupt", "übrigens"]


def fetch_easyleague_html_for_team(team_id: int = 3221) -> str:
    """Fetch data (HTML) from easyleague.

    Calling the correct URL on easyleague (retrieved by peeking at the
    traffic using developer tools in a browser) a HTML snippet of
    rows and cells that make up the table displayed on the website can
    be retrieved. This snippet seems to be processed in JS in the
    browser after retrieval because the raw content is differnen from
    what the browser shows as the source of the website.

    The snipped looks as follows (truncated):

        <?xml version='1.0' encoding='utf-8'?>
        <rows>
            <row id="17888">
            <cell type="link">VBC Hydra^http://indoorvolley.easyleague.ch/index.php?id=teaminfo&amp;amp;teaminfo=1&amp;amp;team_id=3150^_top</cell> <cell type="link">VBC Innova H2^http://indoorvolley.easyleague.ch/index.php?id=teaminfo&amp;amp;teaminfo=1&amp;amp;team_id=3221^_top</cell> <cell>KS Limmattal, Urdorf</cell>   <cell>28.10.2022 0:00</cell></row>
            <row id="17853">
            <cell type="link">VBC Innova H2^http://indoorvolley.easyleague.ch/index.php?id=teaminfo&amp;amp;teaminfo=1&amp;amp;team_id=3221^_top</cell> <cell type="link">VTM Meilen^http://indoorvolley.easyleague.ch/index.php?id=teaminfo&amp;amp;teaminfo=1&amp;amp;team_id=3210^_top</cell>    <cell>Schulhaus am Wasser, Am Wasser 55a, 8049 Zürich</cell>    <cell>03.11.2022 20:45</cell></row>
            [...]
        </rows>
    """  # noqa: E501
    log = logging.getLogger(__name__)
    # url = ("http://indoorvolley.easyleague.ch/"
    #        f"index.php?id=teaminfo&teaminfo=1&team_id={team_id}")
    url = ("http://indoorvolley.easyleague.ch/"
           "index.php?id=connector&modus=10&display=1&teaminfo=1&"
           f"team_id={team_id}")
    log.info(f"fetching Easyleague HTML for team {team_id}")
    log.debug(f"easyleague URL: {url}")

    markup = None

    if team_id:
        r = requests.get(url)
        r.raise_for_status()
        markup = html.fromstring(r.content)

    return markup


def parse_easyleague_html(markup: html) -> List[Matchinfo]:
    """Parse the HTML snippet retrieved by `fetch...`.

    The HTML snipped that `fetch_easyleague_html_for_team` retrieves
    needs parsing/massaging. This function processes each row of the
    data/table and interprets it as a match.

    The `row id` is used as the match id.

    A row looks as follows:

        <row id="17888">
            <cell type="link">VBC Hydra^http://indoorvolley.easyleague.ch/index.php?id=teaminfo&amp;amp;teaminfo=1&amp;amp;team_id=3150^_top</cell> <cell type="link">VBC Innova H2^http://indoorvolley.easyleague.ch/index.php?id=teaminfo&amp;amp;teaminfo=1&amp;amp;team_id=3221^_top</cell> <cell>KS Limmattal, Urdorf</cell>   <cell>28.10.2022 0:00</cell></row>

    The `cell`'s text is split at the caret ("^") sign.

    Missing dates and/or times are replaced by defaults.
    """  # noqa: E501
    log = logging.getLogger(__name__)
    default_hour = 20
    default_minute = 30
    rows = markup.xpath("row")

    m_infos = []
    for row in rows:
        match_no = int(row.get("id"))
        log.debug(f"found match number: {match_no}")
        cells = row.xpath("cell")
        log.debug(f"row has {len(cells)} cells")
        home = _extract_team_name_and_id(cells[0].text)
        log.debug(f"home team information: {home}")
        away = _extract_team_name_and_id(cells[1].text)
        log.debug(f"away team information: {away}")
        venue_id = generate_venue_id(cells[2].text)
        log.debug(f"venue Id: {venue_id}")
        venue_tokens = [token.strip() for token in cells[2].text.split(",")]
        venue_name = venue_tokens[0]
        venue_address = ", ".join(venue_tokens[1:])
        log.debug(f"venue name: {venue_name}")
        log.debug(f"venue address: {venue_address}")
        try:
            date = dateutil.parser.parse(cells[3].text, dayfirst=True)
            if date.tzinfo is None:
                # cet = datetime.timezone(datetime.timedelta(hours=1))
                date = date.replace(tzinfo=default_tz)
            log.debug(f"parsed datetime: {date}")
            time = date.time()
            log.debug(f"parsed time: {time}")
            if time == datetime.time(hour=0, minute=0):
                log.info(f"match time missing for match {match_no}, using "
                         f"default: {default_hour}:{default_minute}")
                date = date.replace(hour=default_hour, minute=default_minute)
        except dateutil.parser.ParserError:
            log.warning(f"error parsing date ({cells[3].text}), using default")
            now = datetime.datetime.now()
            this_year = now.date().year
            date = datetime.datetime(year=this_year,
                                     month=12,
                                     day=31,
                                     hour=default_hour,
                                     minute=default_minute)
        log.debug(f"date: {date}")

        venue_pluscode = ""

        m_info = Matchinfo(
            match_no=match_no,
            date=date,
            home_team=home[1],
            home_team_id=home[0],
            away_team=away[1],
            away_team_id=away[0],
            venue_name=venue_name,
            venue_id=venue_id,
            venue_address=f"{venue_name}, {venue_address}",
            venue_pluscode=venue_pluscode)
        m_infos.append(m_info)
    return m_infos


def generate_venue_id(v_info: str) -> str:
    """Give the "information" about a venue, generate an Id.

    The main reason this function exists is that Easyleague does (seem
    to?) have venue Ids. The venue for a match is simply a text string
    like "Schulhaus am Wasser, Am Wasser 55a, 8049 Zürich" for example.

    Because we want to be able to provide accurate additional
    information about venues in the events (for eaxmple plus codes), we
    need a way to look up that additional information. And that is best
    done via an Id. Hence, we need a way to generate Ids for the
    venues. Ideally, the Id is the same for a given venue even if it is
    provided in a slightly different way on Easyleague.

    The following process is used to calculate the Id:

    1. remove stop words (German)
    2. order tokens (words) alphabetically, keep numbers at front
    3. crate soundex for words, keep numbers
    4. calculate SHA256 hash of concatenated numbers/soundex
    5. use first eight characters of hash as Id
    """
    log = logging.getLogger(__name__)
    # .split() does not remove punctuation
    tokens_raw = [_remove_punctuation(t.lower()) for t in v_info.split()]
    tokens = []
    for token in tokens_raw:
        if token not in stopwords:
            tokens.append(token)
    diff = len(tokens_raw) - len(tokens)
    log.debug(f"removed {diff} stopwords from {tokens_raw}, result: {tokens}")
    tokens.sort()
    tokens_snd = [_soundex(t) for t in tokens]
    log.debug(f"soundexed tokens: {tokens_snd}")
    data = "".join(tokens_snd).encode("utf-8")
    m = hashlib.sha256()
    m.update(data)
    id = m.hexdigest()[:8]
    log.info(f"calculated Id {id} for venue {v_info}")
    return id


def _extract_team_name_and_id(text: str) -> List[str]:
    """Dissect the text of a single `cell` into team info.

    The text of a `cell` with "team data" as received from easyleague
    has the following form:

        VBC Hydra^http://indoorvolley.easyleague.ch/index.php?id=teaminfo&amp;amp;teaminfo=1&amp;amp;team_id=3150^_top

    This function extracts the relevant information from this text: the
    team's name and the Id. In the example "VBC Hydra" (name) and 3150
    (Id). The data is returned as a list with two items: first the Id
    and second the name.
    """  # noqa: E501
    log = logging.getLogger(__name__)
    tokens = text.split("^")
    log.debug(f"extracting team name and Id from text; found {len(tokens)} "
              f"tokens when splitting at the caret ('^'): {tokens}")
    # parse the "middle" part which should be an URL
    url = urllib.parse.urlparse(tokens[1])
    log.debug(f"extracted the url {url} from the 'middle' part of the text")
    # parse the query string part into a dictionary; unfortunately the
    # query string property of the parsed URL contains HTML encoded "&",
    # i.e. "&amp;" which should be replaced before parsing
    query_dict = urllib.parse.parse_qs(url.query.replace("&amp;", "&"))
    log.debug("created the following dictionary from the query string: "
              f"{query_dict}")
    team_id = int(query_dict["team_id"][0])

    # tokens[0] is the team's name, the 'team_id' key in the query string
    # holds the team's Id
    return [team_id, tokens[0]]


# https://stackoverflow.com/a/67197882
def _soundex(s: str) -> str:
    """Calculate soundex for string.

    Copy pasted from SO but changed to leave strings which contain
    numbers alone. So it's probably not strictly soundex any more.
    """
    log = logging.getLogger(__name__)
    if not s:
        return ""

    # leave strings with numbers alone
    # Note: this might not be strict soundex any more!
    if any(c.isdigit() for c in s):
        log.debug(f"found number in string, not calculating soundex: {s}")
        return s

    s = unicodedata.normalize("NFKD", s)
    s = s.upper()

    replacements = (
        ("BFPV", "1"),
        ("CGJKQSXZ", "2"),
        ("DT", "3"),
        ("L", "4"),
        ("MN", "5"),
        ("R", "6"),
    )
    result = [s[0]]
    count = 1

    # find would-be replacment for first character
    for lset, sub in replacements:
        if s[0] in lset:
            last = sub
            break
    else:
        last = None

    for letter in s[1:]:
        for lset, sub in replacements:
            if letter in lset:
                if sub != last:
                    result.append(sub)
                    count += 1
                last = sub
                break
        else:
            if letter != "H" and letter != "W":
                # leave last alone if middle letter is H or W
                last = None
        if count == 4:
            break

    result += "0" * (4 - count)
    return "".join(result)


# https://datagy.io/python-remove-punctuation-from-string/
def _remove_punctuation(s: str) -> str:
    """Remov any punctuation from string."""
    return s.translate(str.maketrans("", "", string.punctuation))
