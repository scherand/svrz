# -*- coding: utf-8 -*-
import logging
import pytest

from datetime import datetime
from datetime import timedelta
from icalendar import vText
from icalendar import vDatetime

from model import Match


@pytest.fixture
def my_uuid():
    return "853a2488-75e5-47e2-979c-2e03aabcc6b3"


@pytest.fixture
def my_uid():
    return "2020-42-1234-6789@woohoo.ch"


@pytest.fixture
def my_vDatetime(my_now):
    return vDatetime(my_now)


@pytest.fixture
def my_event_desc():
    event_desc = "Map: <venue_map>\n"
    event_desc += "\nSportstätte: <sportstaette>\n"
    event_desc += "\nw3w: <w3w_url>\n"
    event_desc += "\nPluscode: <pluscode>\n"
    return event_desc


@pytest.fixture
def my_event_desc_no_w3w():
    event_desc = "Map: <venue_map>\n"
    event_desc += "\nSportstätte: <sportstaette>\n"
    event_desc += "\nPluscode: <pluscode>\n"
    return event_desc


def test_match_initialisation(my_uuid):
    m = Match(uid=my_uuid)
    assert m.uid == my_uuid
    assert m.sequence == 0
    assert m.match_no is None
    assert m.summary == "Match None vs None"
    assert m.date is None
    assert m.home_team is None
    assert m.home_team_id is None
    assert m.away_team is None
    assert m.away_team_id is None
    assert m.venue_name is None
    assert m.venue_id is None
    assert m.venue_address is None
    assert m.venue_url is None
    assert m.event_description == ""

    m = Match()
    assert m.uid == "None-None-None-None@woohoo.ch"
    m.uid = "foobarbaz"
    assert m.uid == "foobarbaz"


def test_match_uid(my_uid):
    m = Match(uid=my_uid)
    assert m.uid == my_uid

    m = Match()
    m.uid = my_uid
    assert m.uid == my_uid
    assert m.match_no == 42
    assert m.home_team_id == 1234
    assert m.away_team_id == 6789


def test_created_timestamp(my_now, my_timezone, my_vDatetime):
    m = Match()
    # check if "created" is within 5 minutes of "now"
    assert (m.created - my_now) < timedelta(minutes=5)
    m.created = my_now
    assert m.created == my_now
    m.created = "2013-11-17T19:23:00+01:00"
    assert m.created == datetime(2013, 11, 17, 19, 23, tzinfo=my_timezone)
    m.created = my_vDatetime
    assert m.created == my_vDatetime.dt


def test_modified_timestamp(my_now, my_timezone, my_vDatetime):
    m = Match()
    # check if "modified" is within 5 minutes of "now"
    assert (m.last_modified - my_now) < timedelta(minutes=5)
    m.last_modified = my_now
    assert m.last_modified == my_now
    m.last_modified = "2013-11-17T19:23:00+01:00"
    assert m.last_modified == datetime(2013, 11, 17, 19, 23,
                                       tzinfo=my_timezone)
    m.last_modified = my_vDatetime
    assert m.last_modified == my_vDatetime.dt


def test_date_timestamp(my_now, my_timezone, my_vDatetime):
    m = Match()
    m.date = my_now
    assert m.date == my_now
    m.date = "2013-11-17T19:23:00+01:00"
    assert m.date == datetime(2013, 11, 17, 19, 23, tzinfo=my_timezone)
    m.date = my_vDatetime
    assert m.date == my_vDatetime.dt


def test_home_team():
    m = Match()
    assert m.home_team is None
    h_team = "HomeTeam"
    m.home_team = h_team
    assert m.home_team == h_team


def test_away_team():
    m = Match()
    assert m.away_team is None
    a_team = "AwayTeam"
    m.away_team = a_team
    assert m.away_team == a_team


def test_venue_address():
    m = Match()
    assert m.venue_address is None
    address_t = "Musterstrasse 99, 9999 Musterfingen"
    m.venue_address = address_t
    assert m.venue_address == address_t
    address_vc = "Glärnischstrasse 3, 8820 Wädenswil"
    m.venue_address = address_vc
    assert m.venue_address == address_vc
    a_vText = vText(address_vc)
    m.venue_address = a_vText
    assert m.venue_address == address_vc


def test_venue_w3w_url():
    m = Match()
    assert m.venue_w3w_url is None
    m._event_desc = "fraz"
    dummy_url = "foobarbaz"
    m.venue_w3w_url = dummy_url
    assert m.venue_w3w_url == dummy_url
    assert m._event_desc is None


def test_venue_map_url():
    m = Match()
    assert m.venue_map_url is None
    m._event_desc = "fraz"
    dummy_url = "foobarbaz"
    m.venue_map_url = dummy_url
    assert m.venue_map_url == dummy_url
    assert m._event_desc is None


def test_venue_spst_url():
    m = Match()
    assert m.venue_spst_url is None
    m._event_desc = "fraz"
    dummy_url = "foobarbaz"
    m.venue_spst_url = dummy_url
    assert m.venue_spst_url == dummy_url
    assert m._event_desc is None


def test_venue_pluscode():
    m = Match()
    assert m.venue_pluscode is None
    m._event_desc = "fraz"
    dummy_url = "foobarbaz"
    m.venue_pluscode = dummy_url
    assert m.venue_pluscode == dummy_url
    assert m._event_desc is None


def test_event_description(my_event_desc, my_event_desc_no_w3w, caplog):
    caplog.set_level(logging.DEBUG)
    m = Match()
    assert m._event_desc is None
    dummy_e_desc = "fraz"
    m.event_description = dummy_e_desc
    assert m._event_desc == dummy_e_desc
    assert m.event_description == dummy_e_desc

    # now, let's set attributes that show up in the event description
    # and check that this works
    # :class:`Match` is not meant to be used like this in real life!
    m = Match()
    m._venue_map_url = "<venue_map>"
    expected_desc = "Map: <venue_map>\n"
    assert m.event_description == expected_desc
    m._venue_spst_url = "<sportstaette>"
    expected_desc += "\nSportstätte: <sportstaette>\n"
    assert m.event_description == expected_desc
    m._venue_w3w_url = "<w3w_url>"
    expected_desc += "\nw3w: <w3w_url>\n"
    assert m.event_description == expected_desc
    m._venue_pluscode = "<pluscode>"
    expected_desc += "\nPluscode: <pluscode>\n"
    assert m.event_description == expected_desc

    m = Match()
    m._venue_spst_url = "<sportstaette>"
    expected_desc = "Sportstätte: <sportstaette>\n"
    assert m.event_description == expected_desc
    m._venue_w3w_url = "<w3w_url>"
    expected_desc += "\nw3w: <w3w_url>\n"
    assert m.event_description == expected_desc
    m._venue_pluscode = "<pluscode>"
    expected_desc += "\nPluscode: <pluscode>\n"
    assert m.event_description == expected_desc

    # and out of order...
    m = Match()
    m._venue_pluscode = "<pluscode>"
    expected_desc = "Pluscode: <pluscode>\n"
    assert m.event_description == expected_desc
    m._venue_w3w_url = "<w3w_url>"
    expected_desc = "w3w: <w3w_url>\n"
    expected_desc += "\nPluscode: <pluscode>\n"
    assert m.event_description == expected_desc

    # finally, test whith setting "proper" event descriptions (simulate
    # loading existing *.ics)
    m = Match()
    m.event_description = my_event_desc
    assert m.event_description == my_event_desc
    venue, na, spst, na, w3w, na, plus, na = my_event_desc.split("\n")
    # using indices here is quite error prone...
    assert m.venue_map_url == venue[5:]
    assert m.venue_spst_url == spst[13:]
    assert m.venue_w3w_url == w3w[5:]
    assert m.venue_pluscode == plus[10:]

    m = Match()
    m.event_description = my_event_desc_no_w3w
    assert m.event_description == my_event_desc_no_w3w
    venue, na, spst, na, plus, na = my_event_desc_no_w3w.split("\n")
    # using indices here is quite error prone...
    assert m.venue_map_url == venue[5:]
    assert m.venue_spst_url == spst[13:]
    assert m.venue_w3w_url is None
    assert m.venue_pluscode == plus[10:]


def test_summary():
    m = Match()
    assert m.home_team is None
    assert m.away_team is None
    summary = "Match Team Home vs Team Away"
    m.summary = summary
    assert m.summary == summary
    assert m.home_team == "Team Home"
    assert m.away_team == "Team Away"


def test_date_from_component_or_string(my_now, my_vDatetime, my_timezone):
    m = Match()
    dt = m.date_from_component_or_string(my_vDatetime)
    assert dt == my_now
    # convert my_now into a string of the form
    # 10.11.2020 22:24:37.998964 (i.e. w/o timezone)
    now_s = my_now.strftime(r"%d.%m.%Y %H:%M:%S.%f")
    dt = m.date_from_component_or_string(now_s)
    assert dt == my_now
    now_naive = my_now.replace(tzinfo=None)
    dt = m.date_from_component_or_string(now_naive)
    assert dt == my_now
    not_ambiguous = datetime(
            year=2020,
            month=2,
            day=25,
            hour=19,
            minute=40,
            tzinfo=my_timezone)
    feb_10 = datetime(
            year=2020,
            month=2,
            day=10,
            hour=19,
            minute=41,
            tzinfo=my_timezone)
    oct_2 = datetime(
            year=2020,
            month=10,
            day=2,
            hour=19,
            minute=42,
            tzinfo=my_timezone)
    not_ambiguous_s = "25.02.20 19:40"
    feb_10_s = "10.02.20 19:41"
    oct_2_s = "02.10.20 19:42"
    assert m.date_from_component_or_string(not_ambiguous_s) == not_ambiguous
    assert m.date_from_component_or_string(feb_10_s) == feb_10
    assert m.date_from_component_or_string(oct_2_s) == oct_2
