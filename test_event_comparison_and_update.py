# -*- coding: utf-8 -*-
import pytest
import copy
import logging
# import svrz_ics

from datetime import datetime
from datetime import timedelta
from model import Match


@pytest.fixture
def first_match_from_ics_simulated_update(my_now):
    m = Match(
              uid="2020-194643-39908-40445@woohoo.ch",
              created=my_now,
              summary="Match VBC Innova H2 vs VBC Züri Unterland H5",
              date="20200925T184500Z",
              home_team="VBC Innova H2",
              away_team="VBC Züri Unterland H5",
              venue_address="Am Wasser 55a, 8049 Zürich"
              )
    m.event_description = ("Map: "
            "http://maps.google.ch/?daddr=Am+Wasser+55a+8049+Zürich\n\n"
            "Sportstätte: https://www.sportstaetten.ch/?ID=32970\n\n"
            "w3w: https://w3w.co/dare.acclaim.feed\n\n"
            "Pluscode: https://plus.codes/8FVC9GW3+7XX\n")
    return m


def test_first_match_from_ics_simulated_update_fixture(
        first_match_from_ics_simulated_update):
    m = first_match_from_ics_simulated_update
    assert m.sequence == 0
    assert m.venue_map_url == \
            "http://maps.google.ch/?daddr=Am+Wasser+55a+8049+Zürich"
    assert m.venue_spst_url == "https://www.sportstaetten.ch/?ID=32970"
    assert m.venue_w3w_url == "https://w3w.co/dare.acclaim.feed"
    assert m.venue_pluscode == "https://plus.codes/8FVC9GW3+7XX"


def test_is_equivalent(match_with_start_time, matches_from_cal):
    assert match_with_start_time.equivalent(match_with_start_time)
    m1 = matches_from_cal[0]
    assert not match_with_start_time.equivalent(m1)


def test_comparison(my_now, matches_from_cal):
    m1 = matches_from_cal[0]
    assert not m1.compare(m1)
    # copy match and change date
    m_copy = copy.copy(m1)
    assert not m1.compare(m_copy)
    m_copy.date = my_now
    assert m1.compare(m_copy)
    # copy match and change home_team
    m_copy = copy.copy(m1)
    m_copy.home_team = "Inexistent Home Team"
    assert m1.compare(m_copy)
    # copy match and change away_team
    m_copy = copy.copy(m1)
    m_copy.away_team = "Inexistent Away Team"
    assert m1.compare(m_copy)
    # copy match and change venue address
    m_copy = copy.copy(m1)
    m_copy.venue_address = "In a country far, far away"
    assert m1.compare(m_copy)
    # copy match and change venue map url
    m_copy = copy.copy(m1)
    m_copy.venue_map_url = "https://www.example.com/example"
    assert m1.compare(m_copy)
    # copy match and change sportstaetten url
    m_copy = copy.copy(m1)
    m_copy.venue_spst_url = "https://www.example.com/example"
    assert m1.compare(m_copy)
    # copy match and change w3w url
    m_copy = copy.copy(m1)
    m_copy.venue_w3w_url = "https://www.example.com/example"
    assert m1.compare(m_copy)
    # copy match and change pluscode
    m_copy = copy.copy(m1)
    m_copy.venue_pluscode = "https://www.example.com/example"
    assert m1.compare(m_copy)


def test_basic_update(my_timezone,
                      my_now,
                      matches_from_cal,
                      first_match_from_ics_simulated_update):
    m1 = matches_from_cal[0]
    m_update = first_match_from_ics_simulated_update
    assert m1.equivalent(m_update)
    assert m_update.sequence == 0
    assert m_update.created == my_now
    assert m_update.last_modified - my_now < timedelta(seconds=1)
    m1_create_date = datetime(year=2020,
                              month=8,
                              day=8,
                              hour=0,
                              minute=25,
                              second=35,
                              tzinfo=my_timezone)
    m1_modify_date = datetime(year=2020,
                              month=9,
                              day=18,
                              hour=10,
                              minute=8,
                              second=20,
                              tzinfo=my_timezone)
    assert m1.created == m1_create_date
    assert m1.last_modified == m1_modify_date
    m_update.update_basic_data(m1)
    assert m_update.sequence == 1
    assert m_update.created == m1_create_date
    assert m_update.last_modified == m1_modify_date


def test_update(matches_from_cal,
                first_match_from_ics_simulated_update,
                caplog):
    caplog.set_level(logging.INFO, logger="svrz_ics")
    m1 = matches_from_cal[0]
    m_update = first_match_from_ics_simulated_update
    m_update.update_basic_data(m1)
    m_update.update(m1)
    assert m1.compare(m_update)
    assert m_update.sequence == 2
